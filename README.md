# Currency rate history
Django application that allows you to save currency rates and display chart based on this data.

![img_1.png](images/img_1.png)

There is a validation on the page:
1. Both fields are required;
2. The rate field must be a positive number;
3. The date field is unique.

![img_3.png](images/img_2.png)

## Built with
- [Django](https://www.djangoproject.com/)
- [Chart.js](https://www.chartjs.org/)
- [Bootstrap](https://getbootstrap.com/)
- [Django-crispy-forms](https://django-crispy-forms.readthedocs.io/en/latest/)

## Creating a virtual environment
1. Use the package manager [pip](https://pip.pypa.io/en/stable/) to install virtualenv.
```bash
pip install virtualenv
```
2. Create a virtual environment.
```bash
python -m virtualenv 'name'
```
3. Activate a virtual environment.
```bash
...\currencyrate\'name'\Scripts\activate
```
## Getting it
If you want to install it from source, grab the git repository from https://bitbucket.org:
```bash
$ git clone git clone https://bidaky@bitbucket.org/bidaky/currency.git
$ cd currencyrate
```
## Installing requirements
All requirements  are stored in requirements.txt and could be installed by command:
```bash
pip install -r requirements.txt.
```

## Using it
To start the server, go to the currencyrate directory (cd currencyrate), if you have not already done so, and run the command:
```bash
python manage.py runserver
```
The command starts the server locally on port 8000. The server only accepts local connections from your computer. Now that it is running, visit the page http://127.0.0.1:8000/ using a browser.

## Testing
Run 
```bash 
python manage.py test
```