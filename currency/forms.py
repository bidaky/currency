from django import forms
from django.core.exceptions import ValidationError
from django.db.models.fields import FloatField
from django.forms import fields, widgets
from .models import Currency


class DateInput(forms.DateInput):
    input_type = 'date'


class CurrencyForm(forms.ModelForm):
    class Meta:
        widgets = {'date': DateInput(attrs={'class': 'form-input'})}
        model = Currency
        fields = ['rate', 'date']
        labels = {
            'rate': ('UAH rate'),
        }

    def clean_rate(self):
        rate = self.cleaned_data['rate']
        if rate <= 0:
            raise ValidationError('Rate must be a positive number.')
        if rate == '':
            raise ValidationError('Input error. This field is required.')
        return rate
