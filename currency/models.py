from django.db import models


class Currency(models.Model):
    rate = models.FloatField()
    date = models.DateField(unique=True)

    class Meta:
        verbose_name_plural = 'Currency rate'
    
    def __str__(self):
        return f'{self.date}," : ",{self.rate}'