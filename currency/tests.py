from datetime import date
from django.test import TestCase
from django.urls import reverse, resolve
from . models import Currency
from . forms import CurrencyForm
from . views import index


class TestForms(TestCase):

    def test_currency_form_valid_data(self):
        form = CurrencyForm(data={
            'rate':'8',
            'date':'2010-10-10'
        })
        self.assertTrue(form.is_valid())

    def test_currency_form_invalid_data(self):
        form = CurrencyForm(data={})
        self.assertFalse(form.is_valid())
        self.assertEquals(len(form.errors), 2)


class TestUrls(TestCase):

    def test_index_is_resolved(self):
        url = reverse('currency-index')
        self.assertEquals(resolve(url).func, index)

class TestViews(TestCase):
    def test_index(self):
        currency = Currency()
        currency.rate = '26.8'
        currency.date = '2021-10-20'
        currency.save()
        record = Currency.objects.get(date='2021-10-20')
        self.assertEqual(record, currency)
