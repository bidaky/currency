from django.core.exceptions import ValidationError
from django.http.response import HttpResponseRedirect
from django.shortcuts import render
from .models import Currency
from .forms import CurrencyForm


def index(request):
    currency_data = Currency.objects.all().order_by('date')
    if request.method == 'POST':
        form = CurrencyForm(request.POST)
        if form.is_valid():
            form.save()
            return HttpResponseRedirect("/")
    else: 
        form = CurrencyForm()
    context = { 'data' : currency_data,
                'form' : form}
    return render(request, 'currency/index.html', context)